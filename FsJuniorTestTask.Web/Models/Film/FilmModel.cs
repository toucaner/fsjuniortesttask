﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace FsJuniorTestTask.Web.Models.Film
{
    public class FilmModel
    {
        #region Properties

        [Display(Name = "Название")]
        [Required(ErrorMessage = "Не указан Название")]
        public string Name { get; set; }

        [Display(Name = "Описание")]
        [Required(ErrorMessage = "Не указан Описание")]
        public string Description { get; set; }

        [Display(Name = "Год выпуска")]
        [StringLength(4)]
        [Required(ErrorMessage = "Не указан Год выпуска")]
        public string ReleaseYear { get; set; }

        [Display(Name = "Режиссёр")]
        [Required(ErrorMessage = "Не указан Режиссёр")]
        public string Director { get; set; }

        public string Creator { get; set; }

        #endregion
    }

    public class FilmEditModel : FilmCreateModel
    {
        #region Properties

        public long Id { get; set; }

        #endregion
    }

    public class FilmCreateModel : FilmModel
    {
        #region Properties

        [Display(Name = "Постер")]
        public IFormFile Poster { get; set; }

        #endregion
    }

    public class FilmDetailModel : FilmEditModel
    {
        #region Properties

        public string PosterPath { get; set; }

        #endregion
    }
}