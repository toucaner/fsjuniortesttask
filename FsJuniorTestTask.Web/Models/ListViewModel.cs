﻿using System.Collections.Generic;
using FsJuniorTestTask.Core.Dto;

namespace FsJuniorTestTask.Web.Models
{
    public class ListViewModel<TDto> where TDto : DtoBase
    {
        #region Properties

        public IEnumerable<TDto> Items { get; set; }

        public PageViewModel PageViewModel { get; set; }

        public FilterViewModel FilterViewModel { get; set; }

        #endregion
    }
}