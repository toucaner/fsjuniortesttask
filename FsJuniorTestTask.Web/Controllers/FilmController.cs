﻿using System;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using FsJuniorTestTask.Bll.Film;
using FsJuniorTestTask.Core.Dto;
using FsJuniorTestTask.Web.Models;
using FsJuniorTestTask.Web.Models.Film;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;

namespace FsJuniorTestTask.Web.Controllers
{
    [Authorize]
    public class FilmController : Controller
    {
        #region Fields

        private readonly IWebHostEnvironment _webHostEnvironment;

        private readonly IFilmService _filmService;

        #endregion

        #region Constructor

        public FilmController(IWebHostEnvironment hostEnvironment, IFilmService filmService)
        {
            _filmService = filmService;
            _webHostEnvironment = hostEnvironment;
        }

        #endregion

        #region Methods

        public async Task<IActionResult> List(string filter, int page = 1, int pageSize = 5)
        {
            var viewModel = new ListViewModel<FilmDto>();
            var filmListResult = await _filmService.GetFilmList(filter, page, pageSize);
            var countResult = await _filmService.GetFilmCount(filter);

            if (!filmListResult.Success)
                ModelState.AddModelError("", filmListResult.Error.Message);

            if (!countResult.Success)
                ModelState.AddModelError("", countResult.Error.Message);

            if (filmListResult.Success && countResult.Success)
            {
                viewModel = new ListViewModel<FilmDto>
                {
                    PageViewModel = new PageViewModel(countResult.Value, page, pageSize),
                    FilterViewModel = new FilterViewModel(filter),
                    Items = filmListResult.Value
                };
            }

            return View(viewModel);
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(FilmCreateModel model)
        {
            if (ModelState.IsValid)
            {
                var posterPath = UploadedFile(model);
                var createResult = await _filmService.CreateFilm(
                    new FilmDto
                    {
                        Name = model.Name,
                        Description = model.Description,
                        Creator = HttpContext.User.Identity.Name,
                        Director = model.Director,
                        ReleaseYear = model.ReleaseYear,
                        PosterPath = posterPath
                    });

                if (!createResult.Success)
                {
                    ModelState.AddModelError("", createResult.Error.Message);
                }
                else
                {
                    return RedirectToAction("List", "Film");
                }
            }

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int filmId)
        {
            var film = await _filmService.GetFilm(filmId);
            if (!film.Success)
                ModelState.AddModelError("", film.Error.Message);

            var model = new FilmEditModel
            {
                Id = film.Value.Id,
                Name = film.Value.Name,
                Director = film.Value.Director,
                Description = film.Value.Description,
                ReleaseYear = film.Value.ReleaseYear,
                Creator = film.Value.Creator
            };

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(FilmEditModel model)
        {
            if (ModelState.IsValid)
            {
                var posterPath = UploadedFile(model);
                var createResult = await _filmService.UpdateFilm(
                    new FilmDto
                    {
                        Id = model.Id,
                        Name = model.Name,
                        Description = model.Description,
                        Creator = HttpContext.User.Identity.Name,
                        Director = model.Director,
                        ReleaseYear = model.ReleaseYear,
                        PosterPath = posterPath
                    });

                if (!createResult.Success)
                {
                    ModelState.AddModelError("", createResult.Error.Message);
                }
                else
                {
                    return RedirectToAction("List", "Film");
                }
            }

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Details(int filmId)
        {
            var film = await _filmService.GetFilm(filmId);
            if (!film.Success)
                ModelState.AddModelError("", film.Error.Message);

            var model = new FilmDetailModel
            {
                Id = film.Value.Id,
                Name = film.Value.Name,
                Director = film.Value.Director,
                Description = film.Value.Description,
                ReleaseYear = film.Value.ReleaseYear,
                PosterPath = film.Value.PosterPath
            };

            return View(model);
        }

        private string UploadedFile(FilmCreateModel model)
        {
            string uniqueFileName = null;

            if (model.Poster != null)
            {
                var uploadsFolder = Path.Combine(_webHostEnvironment.WebRootPath, "images");
                uniqueFileName = Guid.NewGuid() + "_" + model.Poster.FileName;
                var filePath = Path.Combine(uploadsFolder, uniqueFileName);
                using var fileStream = new FileStream(filePath, FileMode.Create);
                model.Poster.CopyTo(fileStream);
            }

            return $"\\images\\{uniqueFileName}";
        }

        #endregion
    }
}
