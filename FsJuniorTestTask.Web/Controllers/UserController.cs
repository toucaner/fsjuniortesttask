﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using FsJuniorTestTask.Bll.User;
using FsJuniorTestTask.Core.Dto;
using FsJuniorTestTask.Web.Models.Account;

namespace FsJuniorTestTask.Web.Controllers
{
    public class UserController : Controller
    {
        #region Fields

        private readonly IUserService _userService;

        #endregion

        #region Constructor

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        #endregion

        #region Methods

        [HttpGet]
        public async Task<IActionResult> Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                var userResult = await _userService.FindUser(model.Email);
                if (!userResult.Success)
                {
                    ModelState.AddModelError("", userResult.Error.Message);
                }
                else
                {
                    if (userResult.Value == null)
                    {
                        var createResult = await _userService.CreateUser(
                            new UserDto
                            {
                                Name = model.Name,
                                Email = model.Email,
                                Password = model.Password
                            });

                        if (!createResult.Success)
                        {
                            ModelState.AddModelError("", userResult.Error.Message);
                        }
                        else
                        {
                            return RedirectToAction("List", "Film");
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "Ошибка регистрации пользователя");
                    }
                }
            }

            return View(model);
        }
    }

    #endregion
}
