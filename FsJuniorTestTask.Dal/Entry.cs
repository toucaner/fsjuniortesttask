﻿using FsJuniorTestTask.Dal.Context;
using FsJuniorTestTask.Dal.Repository.Film;
using FsJuniorTestTask.Dal.Repository.User;
using Microsoft.Extensions.DependencyInjection;

namespace FsJuniorTestTask.Dal
{
    public static class Entry
    {
        #region Methods

        public static IServiceCollection AddDal(this IServiceCollection services)
        {
            services
                .AddSingleton(typeof(IDataContext), typeof(DataContext))
                .AddScoped(typeof(IUserRepository), typeof(UserRepository))
                .AddScoped(typeof(IFilmRepository), typeof(FilmRepository));

            return services;
        }

        #endregion
    }
}