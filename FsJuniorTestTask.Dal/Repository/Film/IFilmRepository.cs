﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace FsJuniorTestTask.Dal.Repository.Film
{
    public interface IFilmRepository : IGenericRepository<Core.Domain.Film>
    {
        #region Methods

        Task<IEnumerable<Core.Domain.Film>> GetAll(string filter, int page, int pageSize, bool withDeleted = false);

        Task<int> GetFilmCount(string filter, bool withDeleted = false);

        #endregion
    }
}