﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FsJuniorTestTask.Dal.Context;
using Microsoft.EntityFrameworkCore;

namespace FsJuniorTestTask.Dal.Repository.Film
{
    public class FilmRepository : GenericRepository<Core.Domain.Film>, IFilmRepository
    {
        #region Constructor

        public FilmRepository(IDataContext context) : base(context)
        {
        }

        #endregion

        #region Methods

        public async Task<IEnumerable<Core.Domain.Film>> GetAll(string filter, int page, int pageSize, bool withDeleted = false)
        {
            var result = GetQuery(filter, withDeleted);
            return await result.Skip((page - 1) * pageSize).Take(pageSize).ToArrayAsync();
        }

        public async Task<int> GetFilmCount(string filter, bool withDeleted = false)
        {
            var result = GetQuery(filter, withDeleted);
            return await result.CountAsync();
        }

        private IQueryable<Core.Domain.Film> GetQuery(string filter, bool withDeleted)
        {
            var result = Entities.AsQueryable();

            if (!string.IsNullOrEmpty(filter))
                result = result.Where(entity => entity.Name.Contains(filter));
            if (!withDeleted)
                result = result.Where(entity => !entity.IsDeleted);

            return result;
        }

        #endregion
    }
}