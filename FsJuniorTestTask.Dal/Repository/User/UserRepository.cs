﻿using System.Threading.Tasks;
using FsJuniorTestTask.Dal.Context;
using Microsoft.EntityFrameworkCore;

namespace FsJuniorTestTask.Dal.Repository.User
{
    public class UserRepository : GenericRepository<Core.Domain.User>, IUserRepository
    {
        #region Constructor

        public UserRepository(IDataContext context) : base(context)
        {
        }

        #endregion

        #region Methods

        public async Task<Core.Domain.User> GetUser(string email)
        {
            return await Entities.FirstOrDefaultAsync(x => x.Email.Equals(email) && !x.IsDeleted);
        }

        #endregion
    }
}