﻿using System.Threading.Tasks;

namespace FsJuniorTestTask.Dal.Repository.User
{
    public interface IUserRepository : IGenericRepository<Core.Domain.User>
    {
        #region Methods

        Task<Core.Domain.User> GetUser(string email);

        #endregion
    }
}