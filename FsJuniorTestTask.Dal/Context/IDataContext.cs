﻿using System.Threading.Tasks;
using FsJuniorTestTask.Core.Domain;
using Microsoft.EntityFrameworkCore;

namespace FsJuniorTestTask.Dal.Context
{
    public interface IDataContext
    {
        #region Methods

        DbSet<TEntity> Set<TEntity>() where TEntity : EntityBase;

        Task<int> SaveChangesAsync();

        #endregion
    }
}