﻿using System;
using System.Threading.Tasks;
using FsJuniorTestTask.Core.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace FsJuniorTestTask.Dal.Context
{
    public class DataContext : DbContext, IDataContext
    {
        #region Constructors

        public DataContext()
        {
            Database.EnsureCreated();
        }

        #endregion

        #region Properties

        public DbSet<User> Users { get; set; }

        public DbSet<Film> Films { get; set; }

        public DbSet<Poster> Posters { get; set; }

        #endregion

        #region Methods

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory)
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json", true)
                .Build();

            optionsBuilder.UseLazyLoadingProxies().UseSqlServer(configuration.GetConnectionString("DefaultConnection"));
            optionsBuilder.LogTo(Console.WriteLine, LogLevel.Error);
        }

        public new DbSet<T> Set<T>() where T : EntityBase => base.Set<T>();

        public new EntityEntry<T> Entry<T>(T entity) where T : EntityBase => base.Entry(entity);

        public Task<int> SaveChangesAsync() => base.SaveChangesAsync();

        #endregion
    }
}