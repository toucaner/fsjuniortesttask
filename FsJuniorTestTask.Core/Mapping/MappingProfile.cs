﻿using AutoMapper;
using FsJuniorTestTask.Core.Domain;
using FsJuniorTestTask.Core.Dto;

namespace FsJuniorTestTask.Core.Mapping
{
    public class MappingProfile : Profile
    {
        #region Constructor

        public MappingProfile()
        {
            CreateMap<User, UserDto>().ReverseMap();
            CreateMap<Film, FilmDto>()
                .ForMember(dest => dest.Creator, opt => opt.MapFrom(src => src.CreatedBy.Email))
                .ForMember(dest => dest.PosterPath, opt => opt.MapFrom(src => src.Poster.Path))
                .ReverseMap();
        }

        #endregion
    }
}