﻿namespace FsJuniorTestTask.Core.Dto
{
    public class FilmDto : DtoBase
    {
        #region Properties

        public string Name { get; set; }

        public string Description { get; set; }

        public string ReleaseYear { get; set; }

        public string Director { get; set; }

        public string Creator { get; set; }

        public string PosterPath { get; set; }

        #endregion
    }
}