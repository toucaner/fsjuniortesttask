﻿namespace FsJuniorTestTask.Core.Dto
{
    public class UserDto : DtoBase
    {
        #region Properties

        public string Email { get; set; }

        public string Name { get; set; }

        public string Password { get; set; }

        #endregion
    }
}