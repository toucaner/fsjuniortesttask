﻿using System.ComponentModel.DataAnnotations;

namespace FsJuniorTestTask.Core.Domain
{
    public class Film : EntityBase
    {
        #region Properties

        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public string ReleaseYear { get; set; }

        [Required]
        public string Director { get; set; }

        [Required]
        public long CreatedById { get; set; }

        [Required]
        public virtual User CreatedBy { get; set; }

        [Required]
        public long PosterId { get; set; }

        [Required]
        public virtual Poster Poster { get; set; }

        #endregion
    }
}