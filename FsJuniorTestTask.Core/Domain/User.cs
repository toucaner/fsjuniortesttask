﻿using System.ComponentModel.DataAnnotations;

namespace FsJuniorTestTask.Core.Domain
{
    public class User : EntityBase
    {
        #region Properties

        [Required]
        public string Email { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Password { get; set; }

        #endregion
    }
}