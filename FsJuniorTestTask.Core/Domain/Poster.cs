﻿using System.ComponentModel.DataAnnotations;

namespace FsJuniorTestTask.Core.Domain
{
    public class Poster : EntityBase
    {
        #region Properties

        [Required]
        public string Path { get; set; }

        #endregion
    }
}