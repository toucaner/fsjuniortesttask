﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using FsJuniorTestTask.Bll.Base;
using FsJuniorTestTask.Bll.User;
using FsJuniorTestTask.Core.Dto;
using FsJuniorTestTask.Dal.Repository.Film;
using FsJuniorTestTask.Utils.Enums;
using FsJuniorTestTask.Utils.Results;
using Microsoft.Extensions.Logging;

namespace FsJuniorTestTask.Bll.Film
{
    public class FilmService : RepositoryServiceBase<Core.Domain.Film>, IFilmService
    {
        #region Fields

        private readonly IFilmRepository _filmRepository;

        private readonly IUserService _userService;

        #endregion

        #region Constructor

        public FilmService(IMapper mapper,
            IFilmRepository repository,
            IUserService userService,
            ILogger<FilmService> logger)
            : base(mapper, repository, logger)
        {
            _filmRepository = repository;
            _userService = userService;
        }

        #endregion

        #region Methods

        public async Task<Result<FilmDto>> GetFilm(long filmId)
        {
            try
            {
                var film = await _filmRepository.Get(filmId);
                var result = Mapper.Map<FilmDto>(film);
                return Result.Ok(result);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                return Result.Fail<FilmDto>(new Error(ErrorCode.OperationExecutionError));
            }
        }

        public async Task<Result<int>> GetFilmCount(string filter, bool withDeleted = false)
        {
            try
            {
                var result = await _filmRepository.GetFilmCount(filter, withDeleted);
                return Result.Ok(result);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                return Result.Fail<int>(new Error(ErrorCode.OperationExecutionError));
            }
        }

        public async Task<Result<IEnumerable<FilmDto>>> GetFilmList(string filter, int page, int pageSize)
        {
            try
            {
                var users = await _filmRepository.GetAll(filter, page, pageSize);
                var result = Mapper.Map<IEnumerable<FilmDto>>(users);
                return Result.Ok(result);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                return Result.Fail<IEnumerable<FilmDto>>(new Error(ErrorCode.OperationExecutionError));
            }

        }

        public async Task<Result> CreateFilm(FilmDto dto)
        {
            try
            {
                var film = Mapper.Map<Core.Domain.Film>(dto);
                var userDto = await _userService.FindUser(dto.Creator);

                if (!string.IsNullOrEmpty(dto.PosterPath))
                {
                    if (film.Poster != null)
                    {
                        film.Poster.Path = dto.PosterPath;
                    }
                    else
                    {
                        film.Poster = new Core.Domain.Poster {Path = dto.PosterPath};
                    }
                }

                film.CreatedById = userDto.Value.Id;

                await Add(film);

                return Result.Ok();
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                return Result.Fail(new Error(ErrorCode.OperationExecutionError));
            }
        }

        public async Task<Result> UpdateFilm(FilmDto dto)
        {
            try
            {
                var film = await Get(dto.Id);
                var userDto = await _userService.FindUser(dto.Creator);

                film.Name = dto.Name;
                film.Description = dto.Description;
                film.ReleaseYear = dto.ReleaseYear;
                film.Director = dto.Director;

                if (!string.IsNullOrEmpty(dto.PosterPath))
                {
                    if (film.Poster != null)
                    {
                        film.Poster.Path = dto.PosterPath;
                    }
                    else
                    {
                        film.Poster = new Core.Domain.Poster {Path = dto.PosterPath};
                    }
                }

                film.CreatedById = userDto.Value.Id;

                await Update(film);

                return Result.Ok();
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                return Result.Fail(new Error(ErrorCode.OperationExecutionError));
            }
        }

        #endregion
    }
}