﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FsJuniorTestTask.Core.Dto;
using FsJuniorTestTask.Utils.Results;

namespace FsJuniorTestTask.Bll.Film
{
    public interface IFilmService
    {
        #region Methods

        Task<Result<FilmDto>> GetFilm(long filmId);

        Task<Result<int>> GetFilmCount(string filter, bool withDeleted = false);

        Task<Result<IEnumerable<FilmDto>>> GetFilmList(string filter, int page, int pageSize);

        Task<Result> CreateFilm(FilmDto film);

        Task<Result> UpdateFilm(FilmDto film);

        #endregion
    }
}