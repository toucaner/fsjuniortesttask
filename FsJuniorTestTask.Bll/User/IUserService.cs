﻿using System.Threading.Tasks;
using FsJuniorTestTask.Core.Dto;
using FsJuniorTestTask.Utils.Results;

namespace FsJuniorTestTask.Bll.User
{
    public interface IUserService
    {
        #region Methods

        Task<Result<UserDto>> FindUser(string email);

        Task<Result<UserDto>> FindUser(string email, string password);

        Task<Result> CreateUser(UserDto user);

        #endregion
    }
}