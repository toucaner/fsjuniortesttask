﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using FsJuniorTestTask.Bll.Base;
using FsJuniorTestTask.Core.Dto;
using FsJuniorTestTask.Dal.Repository.User;
using FsJuniorTestTask.Utils.Enums;
using FsJuniorTestTask.Utils.Helpers;
using FsJuniorTestTask.Utils.Results;
using Microsoft.Extensions.Logging;

namespace FsJuniorTestTask.Bll.User
{
    public class UserService : RepositoryServiceBase<Core.Domain.User>, IUserService
    {
        #region Fields

        private readonly IUserRepository _userRepository;

        #endregion

        #region Constructor

        public UserService(IMapper mapper,
            IUserRepository repository,
            ILogger<UserService> logger)
            : base(mapper, repository, logger)
        {
            _userRepository = repository;
        }

        #endregion

        #region Methods

        public async Task<Result<UserDto>> FindUser(string email)
        {
            try
            {
                var user = await _userRepository.GetUser(email);
                var result = Mapper.Map<UserDto>(user);
                return Result.Ok(result);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                return Result.Fail<UserDto>(new Error(ErrorCode.OperationExecutionError));
            }
        }

        public async Task<Result<UserDto>> FindUser(string email, string password)
        {
            try
            {
                var user = await _userRepository.GetUser(email);
                var isCorrect = user != null && CryptographyHelper.Verify(password, user.Password);
                return isCorrect
                    ? Result.Ok(Mapper.Map<UserDto>(user))
                    : Result.Fail<UserDto>(new Error(ErrorCode.UserNotFound));
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                return Result.Fail<UserDto>(new Error(ErrorCode.OperationExecutionError));
            }
        }

        public async Task<Result> CreateUser(UserDto dto)
        {
            try
            {
                var user = Mapper.Map<Core.Domain.User>(dto);
                user.Password = CryptographyHelper.Hash(user.Password);

                await Add(user);

                return Result.Ok();
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                return Result.Fail(new Error(ErrorCode.OperationExecutionError));
            }
        }

        #endregion
    }
}