﻿using FsJuniorTestTask.Bll.Film;
using FsJuniorTestTask.Bll.User;
using Microsoft.Extensions.DependencyInjection;

namespace FsJuniorTestTask.Bll
{
    public static class Entry
    {
        #region Methods

        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services
                .AddScoped(typeof(IUserService), typeof(UserService))
                .AddScoped(typeof(IFilmService), typeof(FilmService));

            return services;
        }

        #endregion
    }
}